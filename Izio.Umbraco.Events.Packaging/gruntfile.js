﻿module.exports = function (grunt) {

    var name = "Izio.Umbraco.Events";
    var version = "1.0.3";
    var namespace = "Izio.Umbraco";
    var source = "../Izio.Umbraco.Events/";
    var destination = "../Izio.Umbraco.Events.Package/";

    grunt.initConfig({
        copy: {
            package: {
                files: [
                    {
                        expand: true,
                        cwd: source + "bin/",
                        src: [
                            "**/" + namespace + ".*.dll"
                        ],
                        dest: destination + "temp/bin/",
                        flatten: true
                    },
                    {
                        expand: true,
                        cwd: source + "App_Plugins",
                        src: ["**"],
                        dest: destination + "temp/App_Plugins/"
                    },
                    {
                        expand: true,
                        cwd: source + "Scripts",
                        src: ["**"],
                        dest: destination + "temp/Scripts/"
                    },
                    {
                        expand: true,
                        cwd: source + "Css",
                        src: ["**"],
                        dest: destination + "temp/Css/"
                    },
                    {
                        expand: true,
                        cwd: source + "Views",
                        src: ["**"],
                        dest: destination + "temp/Views/"
                    }
                ]
            }
        },
        umbracoPackage: {
            package: {
                src: destination + "temp/",
                dest: destination,
                options: {
                    name: "Izio.Umbraco.Events",
                    version: version,
                    url: "http://www.izio.co.uk",
                    license: "MIT",
                    licenseUrl: "http://www.izio.co.uk/license/mit",
                    author: "Izio",
                    authorUrl: "http://www.izio.co.uk",
                    readme: "<p>Izio Events is an events calendar system for Umbraco that allows you to create any number of calendars and associated events that can be displayed on your site in either month, week or day views.</p>",
                    outputName: name + "." + version + ".zip",
                    manifest: "package.xml"
                }
            }
        },
        clean: {
            package: {
                files: {
                    src: ["../Izio.Umbraco.Events.Package/*", "!../Izio.Umbraco.Events.Package/*.zip"]
                },
                options: {
                    force: true
                }
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-umbraco-package");

    grunt.registerTask("build", ["copy", "umbracoPackage", "clean"]);
    grunt.registerTask("default", ["build"]);
};