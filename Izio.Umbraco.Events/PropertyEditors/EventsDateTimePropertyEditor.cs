﻿using Umbraco.Core.Logging;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web.PropertyEditors;

namespace Izio.Umbraco.Events.PropertyEditors
{
    [DataEditor("Izio.Events.DateTime", EditorType.MacroParameter, "Date/Time", "datepicker", ValueType = "DATETIME", Icon = "icon-time")]
    public class EventsDateTimePropertyEditor : DateTimePropertyEditor
    {
        public EventsDateTimePropertyEditor(ILogger logger) : base(logger)
        {

        }
    }
}