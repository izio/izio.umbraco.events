﻿using Izio.Umbraco.Events.Interfaces;
using Izio.Umbraco.Events.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace Izio.Umbraco.Events.Controllers
{
    /// <summary>
    /// API that provides methods for managing calendars
    /// </summary>
    public class CalendarManagementApiController : UmbracoAuthorizedApiController
    {
        private readonly ICalendarRepository _repository;

        public CalendarManagementApiController(ICalendarRepository calendarRepository)
        {
            _repository = calendarRepository;
        }

        [HttpGet]
        public IEnumerable<Calendar> GetAll()
        {
            return _repository.GetAll();
        }

        [HttpGet]
        public Calendar GetById(int id)
        {
            return _repository.Get(id);
        }

        [HttpGet]
        public Calendar GetByReference(Guid reference)
        {
            return _repository.GetByReference(reference);
        }

        [HttpPost]
        public Calendar Save(Calendar calendar)
        {
            _repository.Save(calendar);

            return calendar;
        }

        [HttpPost]
        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
