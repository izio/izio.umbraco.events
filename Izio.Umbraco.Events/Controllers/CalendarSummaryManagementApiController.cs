﻿using Izio.Umbraco.Events.Interfaces;
using Izio.Umbraco.Events.Models;
using System.Collections.Generic;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace Izio.Umbraco.Events.Controllers
{
    /// <summary>
    /// API that provides methods for managing calendar summaries
    /// </summary>
    public class CalendarSummaryManagementApiController : UmbracoAuthorizedApiController
    {
        private readonly ICalendarSummaryRepository _repository;

        public CalendarSummaryManagementApiController(ICalendarSummaryRepository calendarSummaryRepository)
        {
            _repository = calendarSummaryRepository;
        }

        [HttpGet]
        public IEnumerable<CalendarSummary> GetActive(int limit)
        {
            return _repository.GetActive(limit);
        }

        [HttpGet]
        public CalendarSummary GetById(int id)
        {
            return _repository.Get(id);
        }
    }
}
