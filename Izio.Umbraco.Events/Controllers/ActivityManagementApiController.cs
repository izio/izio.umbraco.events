﻿using Izio.Umbraco.Events.Interfaces;
using Izio.Umbraco.Events.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace Izio.Umbraco.Events.Controllers
{
    /// <summary>
    /// API that provides methods for managing activities
    /// </summary>
    public class ActivityManagementApiController : UmbracoAuthorizedApiController
    {
        private readonly IActivityRepository _repository;

        public ActivityManagementApiController(IActivityRepository activityrRepository)
        {
            _repository = activityrRepository;
        }

        [HttpGet]
        public IEnumerable<Activity> GetAll(int calendarId)
        {
            return _repository.GetAll(calendarId);
        }

        [HttpGet]
        public Activity GetById(int id)
        {
            return _repository.Get(id);
        }

        [HttpGet]
        public IEnumerable<Activity> GetInRange(int calendarId, DateTime start, DateTime end)
        {
            return _repository.GetInRange(calendarId, start, end);
        }

        [HttpGet]
        public Activity GetByReference(Guid reference)
        {
            return _repository.GetByReference(reference);
        }

        [HttpPost]
        public Activity Save(Activity activity)
        {
            _repository.Save(activity);

            return activity;
        }

        [HttpPost]
        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
