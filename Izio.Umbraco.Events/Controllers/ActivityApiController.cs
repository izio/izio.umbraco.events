﻿using Izio.Umbraco.Events.Interfaces;
using Izio.Umbraco.Events.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;
using Umbraco.Web.Mvc;
using Umbraco.Web.WebApi;

namespace Izio.Umbraco.Events.Controllers
{
    [PluginController("Events")]
    public class ActivityApiController : UmbracoApiController
    {
        private IActivityRepository _activityRepository;

        public ActivityApiController(IActivityRepository activityRepository)
        {
            _activityRepository = activityRepository;
        }

        [HttpGet]
        public IEnumerable<Activity> GetInRange(Guid reference, DateTime start, DateTime end)
        {
            return _activityRepository.GetInRange(reference, start, end); ;
        }
    }
}
