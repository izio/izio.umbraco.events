﻿using Izio.Umbraco.Events.Interfaces;
using Izio.Umbraco.Events.ViewModels;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Umbraco.Core.Logging;

namespace Izio.Umbraco.Events.Controllers
{
    [PluginController("Events")]
    public class EventsController : SurfaceController
    {
        private readonly ICalendarRepository _calendarRepository;

        public EventsController(ICalendarRepository calendarRepository)
        {
            _calendarRepository = calendarRepository;
        }

        [ChildActionOnly]
        public ActionResult Render(CalendarRequestViewModel calendarRequest)
        {
            var calendar = _calendarRepository.GetByReference(calendarRequest.Reference);

            if (calendar != null)
            {
                var model = new CalendarViewModel
                {
                    Reference = calendar.Reference,
                    Name = calendar.Name,
                    Description = calendar.Description,
                    Configuration = CalendarViewModel.CreateConfiguration(calendarRequest)
                };

                return PartialView("calendar", model);
            }

            Logger.Error<EventsController>("Invalid calendar requested, reference: {Reference}", calendarRequest.Reference);

            return null;
        }
    }
}
