﻿using Izio.Umbraco.Events.Interfaces;
using Izio.Umbraco.Events.Models;
using System.Collections.Generic;
using Umbraco.Core.Scoping;

namespace Izio.Umbraco.Events.Persistence
{
    public class CalendarSummaryRepository : ICalendarSummaryRepository
    {
        private readonly IScopeProvider _scopeProvider;

        public CalendarSummaryRepository(IScopeProvider scopeProvider)
        {
            _scopeProvider = scopeProvider;
        }

        #region ICalendarSummaryRepository

        public CalendarSummary Get(int id)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var item = database.SingleOrDefault<CalendarSummary>("SELECT Calendar.Id, Calendar.Reference, Calendar.Name, COUNT(Activity.Id) AS Activities, MIN(Activity.Start) AS [NextActivity] FROM izioEventsCalendar AS Calendar LEFT JOIN izioEventsActivity AS Activity ON Calendar.Id = Activity.CalendarId WHERE Calendar.Id = @0 AND Activity.Start > GETDATE() GROUP BY Calendar.Id, Calendar.Reference, Calendar.Name;", id);

                scope.Complete();

                return item;
            }
        }

        public IEnumerable<CalendarSummary> GetActive(int limit)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var items = database.Fetch<CalendarSummary>("SELECT TOP " + limit + " Calendar.Id, Calendar.Reference, Calendar.Name, COUNT(Activity.Id) AS Activities, MIN(Activity.Start) AS [NextActivity] FROM izioEventsCalendar AS Calendar LEFT JOIN izioEventsActivity AS Activity ON Calendar.Id = Activity.CalendarId WHERE Activity.Start > GETDATE() GROUP BY Calendar.Id, Calendar.Reference, Calendar.Name;");

                scope.Complete();

                return items;
            }
        }

        #endregion
    }
}
