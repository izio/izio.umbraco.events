﻿using Izio.Umbraco.Events.Interfaces;
using Izio.Umbraco.Events.Models;
using System;
using System.Collections.Generic;
using Umbraco.Core.Scoping;

namespace Izio.Umbraco.Events.Persistence
{
    /// <summary>
    /// ICalendarRepository implementation for persisting <see cref="Calendar"/>
    /// </summary>
    public class CalendarRepository : ICalendarRepository
    {
        private readonly IScopeProvider _scopeProvider;

        public CalendarRepository(IScopeProvider scopeProvider)
        {
            _scopeProvider = scopeProvider;
        }

        #region ICalendarRepository

        public IEnumerable<Calendar> GetAll()
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var items = database.Fetch<Calendar>("SELECT * FROM izioEventsCalendar");

                scope.Complete();

                return items;
            }
        }

        public Calendar Get(int id)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var item = database.SingleOrDefault<Calendar>("SELECT * FROM izioEventsCalendar WHERE Id = @0", id);

                scope.Complete();

                return item;
            }
        }

        public Calendar GetByReference(Guid reference)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var item = database.SingleOrDefault<Calendar>("SELECT * FROM izioEventsCalendar WHERE Reference = @0", reference);

                scope.Complete();

                return item;
            }
        }

        public Calendar Save(Calendar calendar)
        {
            if (calendar.IsNew || calendar.IsDirty)
            {
                using (var scope = _scopeProvider.CreateScope())
                {
                    var database = scope.Database;

                    database.Save(calendar);
                    

                    scope.Complete();
                }
            }

            return calendar;
        }

        public void Delete(int id)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;

                database.Execute("DELETE FROM izioEventsActivity WHERE CalendarId = @0", id);
                database.Execute("DELETE FROM izioEventsCalendar WHERE Id = @0", id);

                scope.Complete();
            }
        }

        #endregion
    }
}
