﻿using Izio.Umbraco.Events.Interfaces;
using Izio.Umbraco.Events.Models;
using System;
using System.Collections.Generic;
using Umbraco.Core.Scoping;

namespace Izio.Umbraco.Events.Persistence
{
    /// <summary>
    /// IActivityRepository implementation for persisting <see cref="Activity"/>
    /// </summary>
    public class ActivityRepository : IActivityRepository
    {
        private readonly IScopeProvider _scopeProvider;

        public ActivityRepository(IScopeProvider scopeProvider)
        {
            _scopeProvider = scopeProvider;
        }

        #region IActivityRepository

        public IEnumerable<Activity> GetAll(int calendarId)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var activities = database.Fetch<Activity>("SELECT * FROM izioEventsActivity WHERE CalendarId = @0", calendarId);

                scope.Complete();

                return activities;
            }
        }

        public IEnumerable<Activity> GetInRange(int calendarId, DateTime start, DateTime end)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var activities = database.Fetch<Activity>("SELECT * FROM izioEventsActivity WHERE CalendarId = @0 AND ((Start >= @1 AND Start <= @2) OR ([End] >= @1 AND [End] <= @2))", calendarId, start, end);

                scope.Complete();

                return activities;
            }
        }

        public IEnumerable<Activity> GetInRange(Guid calendarReference, DateTime start, DateTime end)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var activities = database.Fetch<Activity>("SELECT Activity.* FROM izioEventsActivity AS Activity INNER JOIN izioEventsCalendar AS Calendar ON Activity.CalendarId = Calendar.Id WHERE Calendar.Reference = @0 AND ((Activity.Start >= @1 AND Activity.Start <= @2) OR (Activity.[End] >= @1 AND Activity.[End] <= @2))", calendarReference, start, end);

                scope.Complete();

                return activities;
            }
        }

        public Activity Get(int id)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var activities = database.SingleOrDefault<Activity>("SELECT * FROM izioEventsActivity WHERE Id = @0", id);

                scope.Complete();

                return activities;
            }
        }

        public Activity GetByReference(Guid reference)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var activities = database.SingleOrDefault<Activity>("SELECT * FROM izioEventsActivity WHERE Reference = @0", reference);

                scope.Complete();

                return activities;
            }
        }

        public Activity Save(Activity activity)
        {
            if (activity.IsValid)
            {
                if (activity.IsNew || activity.IsDirty)
                {
                    using (var scope = _scopeProvider.CreateScope())
                    {
                        var database = scope.Database;

                        database.Save(activity);

                        scope.Complete();
                    }
                }

                return activity;
            }

            throw new ArgumentException("The specified activity is not valid", nameof(activity));
        }

        public void Delete(int id)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;

                database.Execute("DELETE FROM izioEventsActivity WHERE Id = @0", id);

                scope.Complete();
            }
        }

        #endregion
    }
}
