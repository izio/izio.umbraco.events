﻿var app = angular.module("umbraco");

app.requires.push("ui.calendar");
app.controller("Events.DefaultController",
    function ($scope, $http, $location, $filter, notificationsService, navigationService) {

        $scope.loaded = false;
        $scope.calendarSummaries = [];

        // loads the calendar summaries
        $scope.load = function () {
            $http.get("/umbraco/backoffice/api/CalendarSummaryManagementApi/GetActive?limit=12").then(function successCallback(response) {

                $scope.calendarSummaries = response.data;

                $scope.loaded = true;
            }, function errorCallback(response) {

                notificationsService.error("Error", "Failed to load events calendar summaries, please try again later / refresh page");
            });
        };

        $scope.load();
    })
    .controller("Events.CreateController",
        function ($scope, $http, $location, $filter, notificationsService, navigationService) {

            $scope.calendar = {};

            // cancels the calendar creation
            $scope.cancel = function () {
                $scope.create.$dirty = false;
                navigationService.hideMenu();
            };

            // saves the new calendar
            $scope.save = function (calendar) {

                $http.post("/umbraco/backoffice/api/CalendarManagementApi/Save/", calendar).then(function successCallback(response) {

                    navigationService.hideMenu();
                    navigationService.syncTree({ tree: "Events", path: -1 + ',' + response.data.id, forceReload: true, activate: true });
                    notificationsService.success("Success, the calendar " + calendar.name + " has been saved");

                    $location.path("/izioEvents/Events/edit/" + response.data.id);
                }, function errorCallback(response) {
                    notificationsService.error("Error", "Failed to create new events calendar, please try again later / refresh page");
                });
            };
        })
    .controller("Events.EditController",
        function ($scope, $routeParams, $http, notificationsService, navigationService, editorService, uiCalendarConfig) {

            $scope.loaded = false;
            $scope.calendar = null;
            $scope.description = {
                view: "/umbraco/views/propertyeditors/rte/rte.html",
                config: {
                    editor: {
                        toolbar: ["code", "undo", "redo", "cut", "styleselect", "bold", "italic", "alignleft", "aligncenter", "alignright", "bullist", "numlist", "link", "umbmediapicker", "umbmacro", "table", "umbembeddialog"],
                        stylesheets: [],
                        dimensions: { height: 400 }
                    }
                }
            };

            // loads the calendar details
            $scope.load = function () {
                $http.get("/umbraco/backoffice/api/CalendarManagementApi/GetById/" + $routeParams.id).then(function successCallback(response) {

                    $scope.calendar = response.data;
                    $scope.description.value = response.data.description;
                    $scope.loaded = true;
                }, function errorCallback(response) {
                    notificationsService.error("Error", "Failed to load events calendar, please try again later / refresh page");
                });
            };

            // saves the changes to the calendar
            $scope.save = function (calendar) {

                calendar.id = $routeParams.id;
                calendar.description = $scope.description.value;
                calendar.isDirty = true;

                $http.post("/umbraco/backoffice/api/CalendarManagementApi/Save/", calendar).then(function successCallback(response) {
                    navigationService.hideMenu();
                    navigationService.syncTree({ tree: "Events", path: -1 + ',' + response.data.id, forceReload: true, activate: false });
                    notificationsService.success("Success, the calendar " + calendar.name + " has been saved");

                    $scope.calendar = response.data;
                    $scope.edit.$dirty = false;
                }).catch(function (response) {
                    notificationsService.error("Error", "Failed to save events calendar, please try again later / refresh page");
                });
            };

            // gets the activities for the current calendar period
            $scope.getActivities = function (start, end, timezone, callback) {
                $.ajax({
                    url: '/umbraco/backoffice/api/ActivityManagementApi/GetInRange',
                    dataType: 'json',
                    data: {
                        calendarId: $routeParams.id,
                        start: start.toISOString(),
                        end: end.toISOString()
                    },
                    success: function (events) {
                        callback(events);
                    }
                });
            };

            // opens the activity editor window
            $scope.openEditor = function (mode, activity) {

                var options = {
                    view: "/app_plugins/events/backoffice/events/activity.html",
                    title: mode === "edit" ? "Edit Activity" : "New Activity",
                    mode: mode,
                    activity: activity,
                    dateError: false,
                    valid: new Date(activity.start) < new Date(activity.end) && activity.title && activity.title !== "",
                    deleteRequested: false,
                    startPicker: {
                        config: {
                            enableTime: true,
                            useSeconds: false,
                            format: "YYYY-MM-DD HH:mm",
                            icons: {
                                time: "icon-time",
                                date: "icon-calendar",
                                up: "icon-chevron-up",
                                down: "icon-chevron-down"
                            }
                        }
                    },
                    onStartPickerClosed: function (selectedDates, dateStr) {

                        this.activity.start = dateStr;
                        this.validateDates();
                    },
                    endPicker: {
                        config: {
                            enableTime: true,
                            useSeconds: false,
                            format: "YYYY-MM-DD HH:mm",
                            icons: {
                                time: "icon-time",
                                date: "icon-calendar",
                                up: "icon-chevron-up",
                                down: "icon-chevron-down"
                            }
                        }
                    },
                    onEndPickerClosed: function (selectedDates, dateStr) {

                        this.activity.end = dateStr;
                        this.validateDates();
                    },
                    validateDates: function () {

                        var start = new Date(this.activity.start);
                        var end = new Date(this.activity.end);

                        if (start >= end) {

                            this.dateError = true;
                        }
                        else {

                            this.dateError = false;
                        }
                    },
                    descriptionEditor: {
                        view: "/umbraco/views/propertyeditors/rte/rte.html",
                        config: {
                            editor: {
                                toolbar: ["code", "undo", "redo", "cut", "styleselect", "bold", "italic", "alignleft", "aligncenter", "alignright", "bullist", "numlist", "link", "umbmediapicker", "umbmacro", "table", "umbembeddialog"],
                                stylesheets: [],
                                dimensions: { height: 300 }
                            }
                        },
                        value: activity.description
                    },
                    submit: function () {

                        var start = new Date(this.activity.start);
                        var end = new Date(this.activity.end);

                        if (start < end) {

                            activity.description = this.descriptionEditor.value;

                            $http.post("/umbraco/backoffice/api/ActivityManagementApi/Save", activity).then(function successCallback(response) {

                                uiCalendarConfig.calendars.activities.fullCalendar("refetchEvents");

                                notificationsService.success("Success, the activity " + activity.title + " has been saved");
                            }).catch(function (response) {
                                notificationsService.error("Error", "Failed to save activity, please try again later / refresh page");
                            });

                            editorService.close();
                        }
                        else {

                            this.dateError = true;
                        }
                    },
                    requestDelete: function () {

                        this.deleteRequested = true;
                    },
                    cancelDelete: function () {

                        this.deleteRequested = false;
                    },
                    delete: function () {

                        $http.post("/umbraco/backoffice/api/ActivityManagementApi/Delete/" + activity.id).then(function successCallback(response) {

                            uiCalendarConfig.calendars.activities.fullCalendar("refetchEvents");

                            notificationsService.success("Success, the activity " + activity.title + " has been deleted");
                        }).catch(function (response) {
                            notificationsService.error("Error", "Failed to delete activity, please try again later / refresh page");
                        });

                        editorService.close();
                    },
                    close: function () {
                        editorService.close();
                    }
                };
                editorService.open(options);
            };

            /* open activity editor with details of new activity */
            $scope.OnDateClick = function (date) {

                var hour = new Date().getHours();

                var activity = {
                    calendarId: $routeParams.id,
                    start: moment(date).add(hour, "hours").format("YYYY-MM-DD HH:00"),
                    end: moment(date).add(hour + 1, "hours").format("YYYY-MM-DD HH:00"),
                    description: ""
                };

                $scope.openEditor("new", activity);
            };

            /* open activity editor with details of selected activity */
            $scope.OnEventClick = function (date, jsEvent, view) {
                var activity = {
                    id: date.id,
                    calendarId: date.calendarId,
                    reference: date.reference,
                    title: date.title,
                    start: moment(date.start).format("YYYY-MM-DD HH:mm"),
                    end: moment(date.end).format("YYYY-MM-DD HH:mm"),
                    description: date.description,
                    isDirty: true
                };

                $scope.openEditor("edit", activity);
            };

            /* update activity date */
            $scope.OnDrop = function (event, delta, revertFunc, jsEvent, ui, view) {

                var activity = {
                    id: event.id,
                    calendarId: event.calendarId,
                    reference: event.reference,
                    title: event.title,
                    start: new Date(event.start),
                    end: new Date(event.end),
                    description: event.description,
                    isDirty: true
                };

                $http.post("/umbraco/backoffice/api/ActivityManagementApi/Save", activity).then(function successCallback(response) {

                    notificationsService.success("Success, the activity " + event.title + " has been updated");
                }).catch(function (response) {

                    revertFunc();

                    notificationsService.error("Error", "Failed to save activity, please try again later / refresh page");
                });
            };

            /* update activity time */
            $scope.OnResize = function (event, delta, revertFunc, jsEvent, ui, view) {
                var activity = {
                    id: event.id,
                    calendarId: event.calendarId,
                    reference: event.reference,
                    title: event.title,
                    start: new Date(event.start),
                    end: new Date(event.end),
                    description: event.description,
                    isDirty: true
                };

                $http.post("/umbraco/backoffice/api/ActivityManagementApi/Save", activity).then(function successCallback(response) {

                    notificationsService.success("Success, the activity " + event.title + " has been updated");
                }).catch(function (response) {

                    revertFunc();

                    notificationsService.error("Error", "Failed to save activity, please try again later / refresh page");
                });
            };

            /* config object */
            $scope.uiConfig = {
                calendar: {
                    height: 450,
                    editable: true,
                    header: {
                        left: "title",
                        center: "",
                        right: "today month,basicWeek prev,next"
                    },
                    allDaySlot: false,
                    timeZone: "UTC",
                    timeFormat: "H:mm A",
                    dayClick: $scope.OnDateClick,
                    eventClick: $scope.OnEventClick,
                    eventDrop: $scope.OnDrop,
                    eventResize: $scope.OnResize
                }
            };

            /* event sources array*/
            $scope.eventSources = [$scope.getActivities];

            $scope.load();
        })
    .controller("Events.DeleteController",
        function ($scope, $routeParams, $http, $location, notificationsService, navigationService, treeService) {

            $scope.loaded = true;

            // cancels the delete operation
            $scope.cancel = function () {
                navigationService.hideMenu();
            };

            //deletes the calendar and all activities
            $scope.delete = function (id) {
                $http.post("/umbraco/backoffice/api/CalendarManagementApi/Delete/"+ id).then(function successCallback(response) {

                    navigationService.hideMenu();
                    navigationService.syncTree({ tree: "Events", path: -1, forceReload: true, activate: false });
                    treeService.removeNode($scope.currentNode);
                    notificationsService.success("Success, the events calendar has been deleted");

                    $location.path("/izioEvents/Events/default");
                },
                function errorCallback(response) {
                    notificationsService.error("Error",
                        "Failed to delete events calendar, please try again later / refresh page");
                });
            };
        })
    .controller("Events.CalendarPickerController",
        function ($scope, $http) {

            $scope.calendar = [];

            $http.get("/umbraco/backoffice/api/CalendarManagementApi/GetAll/").then(function successCallback(response) {

                $scope.calendars = response.data;
            },
            function errorCallback(response) {
                notificationsService.error("Error",
                    "Failed to retrieve event calendars, please try again later / refresh page");
            });
        })
    .controller("Events.ViewPickerController",
        function ($scope, $http) {

            $scope.views = [{ name: "Month", view: "dayGridMonth" }, { name: "Week", view: "dayGridWeek" }, { name: "Day", view: "timeGridDay" }];
        });
