﻿using Izio.Umbraco.Events.Interfaces;
using Newtonsoft.Json;
using NPoco;
using System;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Izio.Umbraco.Events.Models
{
    [TableName("izioEventsCalendar")]
    public class Calendar : IDatabaseEntity<int>
    {
        #region private properties

        private Guid? _reference;
        private string _name;
        private string _description;

        #endregion

        #region public properties

        /// <summary>
        /// the unique reference of the calendar
        /// </summary>
        [JsonProperty("reference")]
        public Guid Reference
        {
            get
            {
                if (_reference == null)
                {
                    _reference = Guid.NewGuid();
                }

                return _reference.Value;
            }
            set
            {
                if (_reference != null) IsDirty = true;
                _reference = value;
            }
        }

        /// <summary>
        /// the name of the calendar
        /// </summary>
        [JsonProperty("name")]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != null) IsDirty = true;
                _name = value;
            }
        }

        /// <summary>
        /// the description of the calendar
        /// </summary>
        [JsonProperty("description")]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Description
        {
            get { return _description; }
            set
            {
                if (_description != null) IsDirty = true;
                _description = value;
            }
        }

        #endregion

        #region constructors

        public Calendar()
        {
            
        }

        public Calendar(int id, Guid reference, string name, string description)
        {
            Id = id;
            Reference = reference;
            Name = name;
            Description = description;
        }

        #endregion

        #region IDatabaseEntity

        /// <summary>
        /// the unnique identifier of the calendar
        /// </summary>
        [JsonProperty("id")]
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }

        [Ignore]
        [JsonProperty("isNew")]
        public bool IsNew => Id == 0;

        [Ignore]
        [JsonProperty("isDirty")]
        public bool IsDirty { get; set; }

        #endregion
    }
}