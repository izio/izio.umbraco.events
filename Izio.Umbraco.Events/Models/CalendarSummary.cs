﻿using Izio.Umbraco.Events.Interfaces;
using Newtonsoft.Json;
using System;

namespace Izio.Umbraco.Events.Models
{
    public class CalendarSummary : IDatabaseEntity<int>
    {
        [JsonProperty("reference")]
        public Guid Reference { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("activities")]
        public int Activities { get; set; }

        [JsonProperty("nextActivity")]
        public DateTime NextActivity { get; set; }

        #region IDatabaseEntity

        [JsonProperty("id")]
        public int Id { get; private set; }

        [JsonProperty("isNew")]
        public bool IsNew => false;

        [JsonProperty("isDirty")]
        public bool IsDirty => false;

        #endregion
    }
}