﻿using Izio.Umbraco.Events.Interfaces;
using Newtonsoft.Json;
using NPoco;
using System;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Izio.Umbraco.Events.Models
{
    [TableName("izioEventsActivity")]
    public class Activity : IDatabaseEntity<int>
    {
        #region private properties

        private Guid? _reference;
        private string _title;
        private string _description;
        private DateTime _start;
        private DateTime _end;

        #endregion

        #region public properties

        /// <summary>
        /// the id of the parent calendar
        /// </summary>
        [JsonProperty("calendarId")]
        public int CalendarId { get; set; }

        /// <summary>
        /// the unique reference of the activity
        /// </summary>
        [JsonProperty("reference")]
        public Guid Reference
        {
            get
            {
                if (_reference == null)
                {
                    _reference = Guid.NewGuid();
                }

                return _reference.Value;
            }
            private set
            {
                if (_reference != null) IsDirty = true;
                _reference = value;
            }
        }

        /// <summary>
        /// the title of the activity
        /// </summary>
        [JsonProperty("title")]
        public string Title
        {
            get { return _title; }
            set
            {
                if (_title != null) IsDirty = true;
                _title = value;
            }
        }

        /// <summary>
        /// the description of the activity
        /// </summary>
        [JsonProperty("description")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (_description != null) IsDirty = true;
                _description = value;
            }
        }

        /// <summary>
        /// the start date of the activity
        /// </summary>
        [JsonProperty("start")]
        public DateTime Start
        {
            get { return _start; }
            set
            {
                if (_end != DateTime.MinValue && value > _end)
                {
                    throw new ArgumentException("Start time cannot be set after end time.");
                }

                if (_start != DateTime.MinValue) IsDirty = true;
                _start = value;
            }
        }

        /// <summary>
        /// the end date of the activity
        /// </summary>
        [JsonProperty("end")]
        public DateTime End
        {
            get { return _end; }
            set
            {
                if (_start != DateTime.MinValue && value < _start)
                {
                    throw new ArgumentException("End time cannot be set before start time.");
                }

                if (_end != DateTime.MinValue) IsDirty = true;
                _end = value;
            }
        }

        [Ignore]
        public bool IsValid => Start < End;

        #endregion

        #region constructors

        public Activity()
        {
            
        }

        public Activity(int id, Guid reference, string title, string description, DateTime start, DateTime end)
        {
            Id = id;
            Reference = reference;
            Title = title;
            Description = description;
            Start = start;
            End = end;
        }

        #endregion

        #region IDatabaseEntity

        /// <summary>
        /// the unnique identifier of the activity
        /// </summary>
        [JsonProperty("id")]
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; private set; }

        [Ignore]
        [JsonProperty("isNew")]
        public bool IsNew => Id == 0;

        [Ignore]
        [JsonProperty("isDirty")]
        public bool IsDirty { get; set; }

        #endregion
    }
}