﻿using System;

namespace Izio.Umbraco.Events.ViewModels
{
    public class CalendarRequestViewModel
    {
        public Guid Reference { get; set; }

        public bool AllowNavigation { get; set; }

        public bool ShowToday { get; set; }

        public bool AllowViewChange { get; set; }

        public string DefaultView { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool AllDaySlot { get; set; }

        public string Height { get; set; }
    }
}