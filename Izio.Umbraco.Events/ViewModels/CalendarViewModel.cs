﻿using System;

namespace Izio.Umbraco.Events.ViewModels
{
    public class CalendarViewModel
    {
        public Guid Reference { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Configuration { get; set; }

        public static string CreateConfiguration(CalendarRequestViewModel requestModel)
        {
            var plugins = "plugins: ['dayGrid', 'timeGrid'],";
            var header = $"header: {{ left: {(requestModel.AllowNavigation && requestModel.ShowToday ? "'prev,next today'" : requestModel.AllowNavigation ? "'prev,next'" : "''")}, center: 'title', right: {(requestModel.AllowViewChange ? "'dayGridMonth,dayGridWeek,timeGridDay'" : "''")} }},";
            var timeZone = "timeZone: 'UTC',";
            var defaultDate = requestModel.StartDate.HasValue && requestModel.EndDate.HasValue ? $"defaultDate: {requestModel.StartDate.Value.ToString("yyyy-MM-dd")}," : string.Empty;
            var defaultView = $"defaultView: '{requestModel.DefaultView}',";
            var validRange = requestModel.StartDate.HasValue && requestModel.EndDate.HasValue ? $"validRange: {{start: '{requestModel.StartDate.Value.ToString("yyyy-MM-dd")}', end: '{requestModel.EndDate.Value.ToString("yyyy-MM-dd")}'}}," : string.Empty;
            var navigation = $"navLinks: {requestModel.AllowNavigation.ToString().ToLower()},";
            var editable = "editable: false,";
            var height = $"height: {requestModel.Height},";
            var allDaySlot = $"allDaySlot: {requestModel.AllDaySlot.ToString().ToLower()},";
            var scrollTime = "scrollTime: '08:00:00',";
            var eventLimit = "eventLimit: false,";
            var timeFormat = "eventTimeFormat: { hour: '2-digit', minute: '2-digit', meridiem: true },";
            var events = $@"events: {{
                              url: '/umbraco/events/ActivityApi/GetInRange',
                              method: 'GET',
                              extraParams: {{
                                reference: '{requestModel.Reference}'
                              }}
                            }},";
            var eventClick = @"";

            var configuration = $@"{plugins}
                                {header}
                                {defaultDate}
                                {defaultView}
                                {validRange}
                                {navigation}
                                {editable}
                                {height}
                                {allDaySlot}
                                {scrollTime}
                                {eventLimit}
                                {timeFormat}
                                {events}
                                {eventClick}";

            return configuration;
        }
    }
}
