﻿namespace Izio.Umbraco.Events.Interfaces
{
    /// <summary>
    /// interface that defines an object that that can be stored in a database with a primary key of type T
    /// </summary>
    public interface IDatabaseEntity<T>
    {
        /// <summary>
        /// the unique id of the object
        /// </summary>
        T Id { get; }

        /// <summary>
        /// Indicates whether the object is new
        /// </summary>
        bool IsNew { get; }

        /// <summary>
        /// Indicates whether the object has been changes
        /// </summary>
        bool IsDirty { get; }
    }
}
