﻿using Izio.Umbraco.Events.Models;
using System;
using System.Collections.Generic;

namespace Izio.Umbraco.Events.Interfaces
{
    /// <summary>
    /// interface that defines a repository for persisting <see cref="Activity"/> objects
    /// </summary>
    public interface IActivityRepository : IUpdateableRepository<Activity, int>, IRepository<Activity, int>
    {
        /// <summary>
        /// gets all activities
        /// </summary>
        /// <param name="calendarId">the Id of the parent calendar</param>
        /// <returns>an IEnumerable of <see cref="Activity"/> objects</returns>
        IEnumerable<Activity> GetAll(int calendarId);

        /// <summary>
        /// gets all activities in the specified range
        /// </summary>
        /// <param name="calendarId">the Id of the parent calendar</param>
        /// <param name="start">the start date of the range</param>
        /// <param name="finish">the finish date of the range</param>
        /// <returns>an IEnumerable of <see cref="Activity"/> objects</returns>
        IEnumerable<Activity> GetInRange(int calendarId, DateTime start, DateTime finish);

        /// <summary>
        /// gets all activities in the specified range
        /// </summary>
        /// <param name="calendarReference">the reference of the parent calendar</param>
        /// <param name="start">the start date of the range</param>
        /// <param name="finish">the finish date of the range</param>
        /// <returns>an IEnumerable of <see cref="Activity"/> objects</returns>
        IEnumerable<Activity> GetInRange(Guid calendarReference, DateTime start, DateTime finish);

        /// <summary>
        /// gets an activity by the specified reference
        /// </summary>
        /// <param name="reference">the unique reference of the activity</param>
        /// <returns>a <see cref="Activity"/> object</returns>
        Activity GetByReference(Guid reference);
    }
}
