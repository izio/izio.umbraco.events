﻿using Izio.Umbraco.Events.Models;
using System;
using System.Collections.Generic;

namespace Izio.Umbraco.Events.Interfaces
{
    /// <summary>
    /// interface that defines a repository for persisting <see cref="Calendar"/> objects
    /// </summary>
    public interface ICalendarRepository : IUpdateableRepository<Calendar, int>, IRepository<Calendar, int>
    {
        /// <summary>
        /// gets all Calendars
        /// </summary>
        /// <returns>an IEnumerable of <see cref="Calendar"/> objects</returns>
        IEnumerable<Calendar> GetAll();

        /// <summary>
        /// gets a calendar by the specified reference
        /// </summary>
        /// <param name="reference">the unique reference of the calendar</param>
        /// <returns>a <see cref="Calendar"/> object</returns>
        Calendar GetByReference(Guid reference);
    }
}