﻿using Izio.Umbraco.Events.Models;
using System.Collections.Generic;

namespace Izio.Umbraco.Events.Interfaces
{
    /// <summary>
    /// interface that defines a repository for persisting <see cref="CalendarSummary"/> objects
    /// </summary>
    public interface ICalendarSummaryRepository : IRepository<CalendarSummary, int>
    {
        /// <summary>
        /// gets the currently active <see cref="CalendarSummary"/>
        /// </summary>
        /// <param name="limit">the number of <see cref="CalendarSummary"/> to return</param>
        /// <returns>an IEnumerable of <see cref="CalendarSummary"/></returns>
        IEnumerable<CalendarSummary> GetActive(int limit);
    }
}