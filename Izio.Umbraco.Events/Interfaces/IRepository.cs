﻿namespace Izio.Umbraco.Events.Interfaces
{
    /// <summary>
    /// interface that defines a repository for retrieving objects that implement <see cref="IDatabaseEntity{T}"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<TEntity, TKey> where TEntity : IDatabaseEntity<TKey>
    {
        /// <summary>
        /// gets the TEntity specified by TKey
        /// </summary>
        /// <param name="key">the key of the TEntity to retrieve</param>
        /// <returns>the specified TEntity</returns>
        TEntity Get(TKey key);
    }
}
