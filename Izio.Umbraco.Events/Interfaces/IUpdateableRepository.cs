﻿namespace Izio.Umbraco.Events.Interfaces
{
    /// <summary>
    /// interface that defines a repository for updating objects that implement <see cref="IDatabaseEntity{T}"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IUpdateableRepository<TEntity, TKey> where TEntity : IDatabaseEntity<TKey>
    {
        /// <summary>
        /// saves the specified TEntity
        /// </summary>
        /// <param name="entity">the TEntity to save</param>
        /// <returns>the TEntity that was saved</returns>
        TEntity Save(TEntity entity);

        /// <summary>
        /// deletes the specified TEntity
        /// </summary>
        /// <param name="key"></param>
        void Delete(TKey key);
    }
}
