﻿using Izio.Umbraco.Events.Interfaces;
using Izio.Umbraco.Events.Persistence;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace Izio.Umbraco.Events.Installation
{
    public class ServiceComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Register<ICalendarRepository, CalendarRepository>();
            composition.Register<ICalendarSummaryRepository, CalendarSummaryRepository>();
            composition.Register<IActivityRepository, ActivityRepository>();
        }
    }
}
