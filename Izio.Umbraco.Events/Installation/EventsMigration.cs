﻿using Izio.Umbraco.Events.Models;
using Umbraco.Core.Migrations;

namespace Izio.Umbraco.Events.Installation
{

    public class EventsMigration : MigrationBase
    {
        public EventsMigration(IMigrationContext context): base(context)
        {

        }

        public override void Migrate()
        {
            if (!TableExists("izioEventsCalendar"))
            {
                Create.Table<Calendar>().Do();
            }

            if (!TableExists("izioEventsActivity"))
            {
                Create.Table<Activity>().Do();
            }
        }
    }
}
