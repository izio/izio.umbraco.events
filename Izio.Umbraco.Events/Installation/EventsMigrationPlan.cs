﻿using Umbraco.Core.Migrations;

namespace Izio.Umbraco.Events.Installation
{
    public class EventsMigrationPlan : MigrationPlan
    {
        public EventsMigrationPlan() : base("CustomDatabaseTable")
        {
            From(string.Empty).To<EventsMigration>("first-migration");
        }
    }
}