﻿using Umbraco.Core;
using Umbraco.Core.Composing;

namespace Izio.Umbraco.Events.Installation
{
    public class EventsComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Components().Append<EventsComponent>();
        }
    }
}