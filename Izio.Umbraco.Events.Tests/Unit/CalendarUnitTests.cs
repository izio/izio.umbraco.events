﻿using Izio.Umbraco.Events.Models;
using System;
using Xunit;

namespace Izio.Umbraco.Events.Tests.Unit
{
    public class CalendarUnitTests
    {
        [Trait("Category", "unit")]
        public class Properties
        {
            [Fact]
            public void Reference_Is_Generated()
            {
                // create new calendar
                var calendar = new Calendar();

                // validate guid
                Assert.IsType<Guid>(calendar.Reference);
            }

            [Fact]
            public void Reference_Does_Not_Change()
            {
                // create new calendar
                var calendar = new Calendar();

                // validate guid
                Assert.Equal(calendar.Reference, calendar.Reference);
            }

            [Fact]
            public void New_Calendar_Is_Flagged_As_New()
            {
                // create new calendar
                var calendar = new Calendar();

                // validate is new
                Assert.True(calendar.IsNew);
            }

            [Fact]
            public void Reference_Is_Set()
            {
                // create new calendar
                var calendar = new Calendar();

                // validate reference
                Assert.NotEqual(Guid.Empty, calendar.Reference);
            }

            [Fact]
            public void Name_Is_Set()
            {
                // create new calendar
                var calendar = new Calendar();
                var name = "calendar";

                // set name
                calendar.Name = name;
                
                // validate name
                Assert.Equal(name, calendar.Name);
            }

            [Fact]
            public void Description_Is_Set()
            {
                // create new calendar
                var calendar = new Calendar();
                var description = "calendar";

                // set description
                calendar.Description = description;

                // validate description
                Assert.Equal(description, calendar.Description);
            }

            [Fact]
            public void Editing_Properties_Of_New_Calendar_Does_Not_Flag_As_Dirty()
            {
                // create new calendar
                var calendar = new Calendar();

                // set properties
                calendar.Name = "calendar";
                calendar.Description = "calendar";

                // validate description
                Assert.False(calendar.IsDirty);
            }

            [Fact]
            public void Editing_Name_Of_Existing_Calendar_Does_Flag_As_Dirty()
            {
                // create new calendar
                var calendar = new Calendar(1, Guid.NewGuid(), "name", "description");

                // set properties
                calendar.Name = "calendar";

                // validate is dirty
                Assert.True(calendar.IsDirty);
            }

            [Fact]
            public void Editing_Description_Of_Existing_Calendar_Does_Flag_As_Dirty()
            {
                // create new calendar
                var calendar = new Calendar(1, Guid.NewGuid(), "name", "description");

                // set properties
                calendar.Description = "calendar";

                // validate is dirty
                Assert.True(calendar.IsDirty);
            }
        }
    }
}
