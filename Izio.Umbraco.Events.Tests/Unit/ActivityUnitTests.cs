﻿using Izio.Umbraco.Events.Models;
using System;
using Xunit;

namespace Izio.Umbraco.Events.Tests.Unit
{
    public class ActivityUnitTests
    {
        [Trait("Category", "unit")]
        public class Properties
        {
            [Fact]
            public void New_Activity_Is_Flagged_As_New()
            {
                // create new activity
                var activity = new Activity();

                // validate is new
                Assert.True(activity.IsNew);
            }

            [Fact]
            public void CalendarId_Is_Set()
            {
                // create new activity
                var activity = new Activity();
                var calendarId = 1;

                // set calendar id
                activity.CalendarId = calendarId;

                // validate calendar id
                Assert.Equal(calendarId, activity.CalendarId);
            }

            [Fact]
            public void Reference_Is_Generated()
            {
                // create new calendar activity
                var activity = new Activity();

                // validate guid
                Assert.IsType<Guid>(activity.Reference);
            }

            [Fact]
            public void Reference_Does_Not_Change()
            {
                // create new calendar activity
                var activity = new Activity();

                // validate guid
                Assert.Equal(activity.Reference, activity.Reference);
            }

            [Fact]
            public void Reference_Is_Set()
            {
                // create new calendar activity
                var activity = new Activity();

                // validate reference
                Assert.NotEqual(Guid.Empty, activity.Reference);
            }

            [Fact]
            public void Title_Is_Set()
            {
                // create new calendar activity
                var activity = new Activity();
                var title = "activity";

                // set title
                activity.Title = title;
                
                // validate title
                Assert.Equal(title, activity.Title);
            }

            [Fact]
            public void Description_Is_Set()
            {
                // create new activity
                var activity = new Activity();
                var description = "activity";

                // set description
                activity.Description = description;

                // validate description
                Assert.Equal(description, activity.Description);
            }

            [Fact]
            public void Start_Is_Set()
            {
                // create new activity
                var activity = new Activity();
                var start = DateTime.Now;

                // set start
                activity.Start = start;

                // validate start
                Assert.Equal(start, activity.Start);
            }

            [Fact]
            public void Finish_Is_Set()
            {
                // create new activity
                var activity = new Activity();
                var end = DateTime.Now;

                // set end
                activity.End = end;

                // validate end
                Assert.Equal(end, activity.End);
            }

            [Fact]
            public void Start_Cannot_Be_Set_After_Finish()
            {
                // create new activity
                var activity = new Activity();
                var start = DateTime.Now.AddHours(1);
                var end = DateTime.Now;

                //set end 
                activity.End = end;

                // set start
                var exception = Record.Exception(() => activity.Start = start);

                Assert.NotNull(exception); // an exception was thrown
                Assert.IsType<ArgumentException>(exception); // it was of the correct type
                Assert.Contains("Start time cannot be set after end time.", exception.Message); // the message is correct
            }

            [Fact]
            public void Finish_Cannot_Be_Set_Before_Start()
            {
                // create new activity
                var activity = new Activity();
                var start = DateTime.Now.AddHours(1);
                var end = DateTime.Now;

                //set start 
                activity.Start = start;

                // set end
                var exception = Record.Exception(() => activity.End = end);

                Assert.NotNull(exception); // an exception was thrown
                Assert.IsType<ArgumentException>(exception); // it was of the correct type
                Assert.Contains("End time cannot be set before start time.", exception.Message); // the message is correct
            }

            [Fact]
            public void Editing_Properties_Of_New_Event_Does_Not_Flag_As_Dirty()
            {
                // create new activity
                var activity = new Activity();

                // set properties
                activity.Title = "activity";
                activity.Description = "activity";

                // validate description
                Assert.False(activity.IsDirty);
            }

            [Fact]
            public void Editing_Title_Of_Existing_Event_Does_Flag_As_Dirty()
            {
                // create new activity
                var activity = new Activity(1, Guid.NewGuid(), "title", "description", new DateTime(), new DateTime());

                // set properties
                activity.Title = "activity";

                // validate description
                Assert.True(activity.IsDirty);
            }

            [Fact]
            public void Editing_Description_Of_Existing_Event_Does_Flag_As_Dirty()
            {
                // create new activity
                var activity = new Activity(1, Guid.NewGuid(), "title", "description", new DateTime(), new DateTime());

                // set properties
                activity.Description = "activity";

                // validate description
                Assert.True(activity.IsDirty);
            }
        }
    }
}
